﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scorekeeper : MonoBehaviour {

    
    public int scorePerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;
    public Text[] winText;
    bool gameWin = false;


    
	// Use this for initialization
	void Start () {
        Goal[] goals = FindObjectsOfType<Goal>();

        for (int i = 0; i < goals.Length; i++)
        {
            goals[i].scoreGoalEvent += OnScoreGoal;
        }
 for (int i = 0; i<score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
      
                	
	}
	
    public void OnScoreGoal(int player)
    {
        score[player] += scorePerGoal;
        scoreText[player].text = score [player].ToString();
        Debug.Log("Player " + player + ": " + score [player]);
        
        if (score[player] == 3)
        {
            
          winText[player].text = "Player "  + player + " Wins";
            gameWin = true;
            Time.timeScale = 0;
        }
        
    }

	// Update is called once per frame
	void Update () {
	
	}

    
}
