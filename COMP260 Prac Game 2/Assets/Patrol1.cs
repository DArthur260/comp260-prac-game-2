﻿using UnityEngine;
using System.Collections;

public class Patrol1 : MonoBehaviour {


    public Transform farEnd;
    public Transform startend;
    private Vector3 frometh;
    private Vector3 untoeth;
    private float secondsForOneLength = 3f;

    void Start()
    {
        frometh = startend.position;
        untoeth = farEnd.position;
    }

    void Update()
    {
        transform.position = Vector3.Lerp(frometh, untoeth,
         Mathf.SmoothStep(0f, 1f,
          Mathf.PingPong(Time.time / secondsForOneLength, 1f)
        ));
    }
}