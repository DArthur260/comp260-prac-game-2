﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeyboard : MonoBehaviour
{

    // Use this for initialization
    private Rigidbody rigidbody;
    public float speed = 20f;
    public float force = 10f;
    Vector3 pos;

    /*   private Vector3 GetMousePosition()
       {
           Ray ray =
   Camera.main.ScreenPointToRay(Input.mousePosition);

           Plane plane = new Plane(Vector3.forward, Vector3.zero);
           float distance = 0;
           plane.Raycast(ray, out distance);
           return ray.GetPoint(distance);
       }
       */
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

  
    void FixedUpdate()
    {
        {

            Vector3 dir = pos - rigidbody.position;
            dir.x = Input.GetAxis("Horizontal");
            dir.y = Input.GetAxis("Vertical");
            Vector3 vel = dir.normalized * speed;
            float move = speed * Time.fixedDeltaTime;
            float distToTarget = dir.magnitude;
            if (move > distToTarget)
            {
                vel = vel * distToTarget / move;

            }
            rigidbody.velocity = vel;
            rigidbody.AddForce(dir.normalized * force);
        }
    }

}

